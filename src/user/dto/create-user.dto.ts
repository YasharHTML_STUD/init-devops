import {IsEmail, IsString, IsStrongPassword} from "class-validator"
export class CreateUserDto {
  @IsString()
  name: string;

  @IsString() // use IsEmail
  email: string;

  @IsString() // password validation not wokring, use IsStrongPassword
  password: string;
}
