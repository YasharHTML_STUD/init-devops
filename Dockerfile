FROM node:18-alpine

WORKDIR /app

COPY package.json .
RUN yarn install --production

COPY ./dist/main.js server.js

CMD [ "node", "server.js" ]